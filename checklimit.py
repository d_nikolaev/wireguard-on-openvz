#!/usr/local/src/scripts/checklimit/venv/bin/python
# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import settings
import datetime
from checkpeers import checkpeers

key = settings.key
value = settings.value

def getbandwidth():
    """
    Получаем по API bandwidth usage in Bytes, 
    а именно HTML вида:
        <bw>536870912000,11757850542,525113061458,2</bw> (bandwidth usage in Bytes)
        <status>success</status>
        <statusmsg></statusmsg>
        <hostname>HOSTNAME</hostname>
        <ipaddress>INTERNALIP</ipaddress>
        <vmstat></vmstat>
    содержимое тега <bw> — total,used,free,percentused
    """
    try:
        url = "https://solusvm.gullo.me/api/client/command.php"
        action = 'status'
        bw = 'true'
        url += '?key=' + key +'&hash=' + value + '&action=' + action + '&bw=' + bw
        html = requests.request("GET", url).text
        soup = BeautifulSoup(html, 'lxml')
        return soup
    except:
        print("Error API")
        exit()

def getfreelimit():
    """
    Принимает html-код, парсит его, отдаёт количество оставшихся в месяце байт 
    """
    soup = getbandwidth()
    try:
        limit = soup.find('bw').text.split(sep=',')
        total = limit[0]
        used = limit[1]
        free = limit[2]
        percentused = limit[3]
        free = int (free)
        return free
    except:
        free = None
        return free

def getdaysleft():
    """
    Сколько осталось дней в месяце: последнее число текущего месяца минус сегодняшнее число
    """
    now = datetime.datetime.now()
    num_days=(31,28,31,30,31,30,31,31,30,31,30,31)
    daysleft = (num_days[now.month-1]) - now.day
    return daysleft

def calclimit():
    """
    Посчитать лимит трафика в день на каждого пира
    """
    free = getfreelimit()
    peers = checkpeers()
    days = getdaysleft()
    limit = int(free / days / peers)
    return limit

def main():
    print("Free limit: " + str(getfreelimit()) + " bytes")
    print("Limit of day: " + str(calclimit()) + " bytes")

if __name__ == '__main__':
    main()
