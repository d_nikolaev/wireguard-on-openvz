#!/bin/bash

if [ $# -eq 0 ]
then
echo "Укажите имя клиента! Пример: add_user.sh new-client"
else
userName="$1"
token=<TOKEN>
chat_id=<CHAT_ID>
mkdir client_"$userName"
wg genkey | tee /etc/wireguard/client_"$userName"/"$userName"_privatekey | wg pubkey | tee /etc/wireguard/client_"$userName"/"$userName"_pubkey
privatekey=$(cat /etc/wireguard/"client_$userName"/"$userName"_privatekey)
pubkey=$(cat /etc/wireguard/client_"$userName"/"$userName"_pubkey)
currentIPv4="10.123.0."$(expr $(cat /etc/wireguard/last_ipv4 | cut -d '.' -f 4) + 1)
currentIPv6="fd42:42:42::"$(expr $(cat /etc/wireguard/last_ipv6 | cut -d ':' -f 5) + 1)
domain=$(curl ifconfig.me)
port=$(cat /etc/wireguard/wg0.conf | grep ListenPort | awk '{print $3}')
serverPublicKey=$(cat /etc/wireguard/publickey)
echo "[Peer]
# $userName
PublicKey = $pubkey
AllowedIPs = $currentIPv4/32,$currentIPv6/128
" >> wg0.conf
wg set wg0 peer $pubkey allowed-ips $currentIPv4/32,$currentIPv6/128
echo "[Interface]
PrivateKey = $privatekey
Address = $currentIPv4/32, $currentIPv6/80
DNS = 1.1.1.1, 1.0.0.1, 2001:4860:4860::8888
[Peer]
PublicKey = $serverPublicKey
Endpoint = $domain:$port
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 25" >> /etc/wireguard/client_"$userName"/"$userName".conf
qrencode -t PNG -o /etc/wireguard/client_"$userName"/"$userName".png < /etc/wireguard/client_"$userName"/"$userName".conf
curl -F document=@"/etc/wireguard/client_"$userName"/"$userName".png" https://api.telegram.org/bot"$token"/sendDocument?chat_id="$chat_id"
echo $currentIPv4 > last_ipv4
echo $currentIPv6 > last_ipv6
fi
