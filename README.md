# WireGuard on OpenVZ
### Пост сделан на основе статей:
1. [WireGuard on OpenVZ/LXC](https://d.sb/2019/07/wireguard-on-openvz-lxc)
2. [How to see debug logs for WireGuard](https://serverfault.com/questions/1020279/how-to-see-debug-logs-for-wireguard-e-g-to-see-authentication-attempts)
3. [How do I fix wget on an HTTP url not trusted errors in kali?](https://unix.stackexchange.com/questions/334905/how-do-i-fix-wget-on-an-http-url-not-trusted-errors-in-kali)

[WireGuard](https://www.wireguard.com/) — это захватывающая, новая и чрезвычайно простая система VPN, использующая современную криптографию. Его реализация для Linux работает в ядре, что обеспечивает значительный прирост производительности по сравнению с традиционными реализациями VPN в пользовательском пространстве.  
Модуль ядра WireGuard великолепен, но иногда вы не сможете установить новые модули ядра. В качестве примера можно привести VPS, использующий OpenVZ или LXC. В этих случаях мы можем использовать [wireguard-go](https://git.zx2c4.com/wireguard-go/about/) , реализацию WireGuard в пользовательском пространстве.  Это та же реализация, которая используется в MacOS, Windows и мобильных приложениях WireGuard. Эта реализация медленнее, чем модуль ядра, но все же достаточно быстрая.  

## Подготовка сервера и установка Wireguard и компонентнов
**Внимание! Инструкция ниже для настройки на Debian 9. С другими дистрибутивами может не работать.**
На Debian 10, на Centos 7, на Centos 8 не заработало.  
### Обновление сервера  
`apt-get update -y`  
`apt-get upgrade -y`  

### Русификация сервера  (необязательно)
#### Установка русской локали  
Запустить:  
`dpkg-reconfigure locales`  
И выбрать:  
`> ru_RU.UTF-8`  

#### Установка московского времени  
Сделать резервную копию текущей timezone  
`mv /etc/localtime /etc/localtime.bak`  

В каталоге /usr/share/zoneinfo/ найти Москву и сделать ссылку  
`ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime`  

В итоге:  
`$ date`  
`Сб окт 13 02:59:46 MSK 2018`  

### Установка обновлений  
`echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable.list`   
`printf 'Package: *\nPin: release a=unstable\nPin-Priority: 90\n' > /etc/apt/preferences.d/limit-unstable`   
`apt update`  

### Перезагрузка сервера (не обязательно, но желательно, чтобы был русский язык)  
`reboot`  

### Установка iptables, если не установлена  
Проверить:  
`iptables -L`  

Если службы нет, установить:  
`apt-get install iptables -y`  

### Установка необходимых для дальнейшей работы программ (mc и htop по желанию)  
`apt-get install -y sudo make vim mc htop curl git cron`  
`apt install qrencode -y`  

### Установка WireGuard tools (wg-quick)  
`apt install wireguard-tools --no-install-recommends`  

### Установка пакета ca-certificates  
`apt-get install ca-certificates -y`  

### Установка GO  
К сожалению, так как _wireguard-go_ не упакован для Debian, нужно скомпилировать его самостоятельно. Чтобы скомпилировать его,  сначала нужно установить последнюю версию языка программирования Go (на данный момент версия 1.13.4):  
`cd /tmp`  
`wget https://dl.google.com/go/go1.13.4.linux-amd64.tar.gz`   
`tar zvxf go1.13.4.linux-amd64.tar.gz`  
`mv go /opt/go1.13.4`  
`ln -s /opt/go1.13.4/bin/go /usr/local/bin/go`  

Теперь при запуске `go version` должен отображаться номер версии.  

### Скомпилировать wireguard-go  
Теперь, когда есть Go, можно скачать и скомпилировать wireguard-go. Загрузить последнюю версию выпуска:  
`cd /usr/local/src`  
`wget https://git.zx2c4.com/wireguard-go/snapshot/wireguard-go-0.0.20191012.tar.xz --no-check-certificate`  
`tar xvf wireguard-go-0.0.20191012.tar.xz`  
`cd wireguard-go-0.0.20191012`   

Если Вы работаете в системе с ограниченным объемом оперативной памяти (например, на VPS LowEndSpirit объемом 256 МБ или меньше), Вам потребуется внести небольшие изменения в код _wireguard-go_, чтобы он использовал меньше оперативной памяти. Откройте _device/queueconstants_default.go_ и замените это:  

`MaxSegmentSize             = (1 << 16) - 1 // largest possible UDP datagram`   
`PreallocatedBuffersPerPool = 0 // Disable and allow for infinite memory growth`  

На эти значения (взято из _device/queueconstants_ios.go_):  
`MaxSegmentSize             = 1700`  
`PreallocatedBuffersPerPool = 1024`  
Это заставит службу использовать фиксированный объем ОЗУ (максимум ~ 20 МБ), а не позволит использовать память бесконечно.  

Теперь можно запустить компиляцию:  
`make`  
`cp wireguard-go /usr/local/bin`  

Команда `wireguard-go --version` должна сработать и показать номер версии.


## Настройка конфигурации  
### Скопировать проект с файлами 
### wg0.conf  
Перейти в каталог /etc/wireguard:  
`cd /etc/wireguard`  

Скопировать в каталог проект из git, переместить файлы в /etc/wireguard, удалить лишнее:  
`git clone https://gitlab.com/d_nikolaev/wireguard-on-openvz.git`  
`mv ./wireguard-on-openvz/* .`  
`rm -rf wireguard-on-openvz/`  

Создать приватный и публичный ключи сервера:     
`wg genkey | tee privatekey | wg pubkey | tee publickey`   

Установить права на приватный ключ:  
`chmod 600 privatekey`  

Создать и открыть на редактирование файл конфигурации wg0.conf:  
`touch wg0.conf`  
`vim wg0.conf`  

Вставить в файл следующее содержимое:  
`[Interface]`  
`Address = 10.123.0.1/24,fd42:42:42::1/80`  
`PrivateKey = <PRIVATE KEY OF SERVER>`  
`ListenPort = <PORT>`  
`PostUp = iptables -t nat -A POSTROUTING -o venet0 -j MASQUERADE; ip6tables -t nat -A POSTROUTING -o venet0 -j MASQUERADE`  
`PostDown = iptables -t nat -D POSTROUTING -o venet0 -j MASQUERADE; ip6tables -t nat -D POSTROUTING -o venet0 -j MASQUERADE`  
`SaveConfig = false`  

Вместо _PRIVATE KEY OF SERVER_ вставить содержимое файла _privatekey_.  
Вместо _PORT_ вставить номер порта, доступного на данном OpenVZ.  

### IP форвардинг  
Настроить IP форвардинг:  
`echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf`  
`sysctl -p`  

### Включить systemd демон с wireguard:  
`systemctl enable wg-quick@wg0.service`  
`systemctl start wg-quick@wg0.service`  
`systemctl status wg-quick@wg0.service`  

### Создать тестовый ключ клиента и добавить его публичный ключ в wg0.conf:  
Создать каталоги для каждого клиента:  
`mkdir client_test`  

Сгенерировать ключи:  
`wg genkey | tee /etc/wireguard/client_test/test_privatekey | wg pubkey | tee /etc/wireguard/client_test/test_publickey`  

Открыть на редактирование файл wg0.conf:  
`vim wg0.conf`  

Вставить внизу файла следующее:  
` `  
`[Peer]`  
`PublicKey = <PUBLIC KEY OF TEST_CLIENT>`  
`AllowedIPs = 10.123.0.2/32,fd42:42:42::2/128`  

Вместо _PUBLIC KEY OF TEST_CLIENT_  вставить скопированный ключ.  

Установить права на приватные ключи:  
`chmod 600 ./client_*/*privatekey`  

### Перезагрузить systemd сервис с wireguard  
`systemctl restart wg-quick@wg0`  
`systemctl status wg-quick@wg0`  

### Добавление пользователя с помощью скрипта  
Сделать файл создания нового юзера add_user.sh исполняемым:  
`chmod +x add_user.sh`  

В файле указать вместо _TOKEN_ - токен бота в телеграм, вместо _CHAT_ID_ - id чата с ботом в телеграм.

Создать файлы last_ipv4 и last_ipv6, в которых будут храниться последние использованные IP-адреса пиров:  
`touch last_ipv4 && touch last_ipv6`  

Вписать в них последние использованные IP-адреса (10.123.0.2 и fd42:42:42::2).  

После запуска скрипта будет создан каталог client_$username, в котором будут хранится публичный и приватный ключи, файл конфига пользователя, а также QR-код, который можно использовать для добавления конфига в пользовательской программе. QR-код также отправляется в телеграм. Формат команды для запуска скрипта:  
`./add_user.sh new-client`  

### Настроить логирование
Вероятнее всего, в OpenVZ нет службы _rsyslog_, не создаются файлы _syslog_ и _messages_. Создадим свой лог _/var/log/wg/log_ для wg.  
`touch /var/logwg.log`  
`cd /usr/local/src`  
`mkdir scripts && cd scripts`  
`touch wg-log.sh`  
`chmod +x wg-log.sh`  
`vim wg-log.sh`  

Вставить в файл следующее:  
`#!/bin/bash`  
`wg show all dump | grep 10.123 | awk 'BEGIN {}; {if (systime()-$6 <180 ) printf "%s %s - %s, %s sec., %.2f MiB used \n", strftime("%m-%d-%Y %H:%M:%S", systime()), $5, $4, systime()-$6, $7/1024/1024+$8/1024/1024 } ; END {}' >> /var/log/wg.log`  

Добавить в cron новое задание, которое будет запускаться раз в 3 минуты:  
`crontab -e`  
И добавить строку:  
`*/3  * * * * /usr/local/src/scripts/wg-log.sh`  

Перезапустить службу cron:  
`systemctl restart cron`  
`systemctl status cron`  

### Настройка автобана пользователя, превысевшего лимит
На OpenVZ часто используется ограничение по трафику. Чтобы как-то можно было контролировать его, предлагается настроить автобан пира до наступления следующих суток.  
![](scheme.png)  
 
Файлы должны располагаться в каталоге `/usr/local/src/scripts/checklimit`. Для этого создать каталог, перейдём в него:  
`mkdir /usr/local/src/scripts/checklimit && cd /usr/local/src/scripts/checklimit`  

Переместить скрипты из /etc/wireguard в текущий каталог, лишнее удалить:  
`mv /etc/wireguard/*.py .`  
`mv /etc/wireguard/requirements.txt .`  
`rm -rf /etc/wireguard/README.md`  
`rm -rf /etc/wireguard/scheme.png`  

Установить Python 3 версии на сервер, виртуальное окружение для него, запустить виртуальное окружение:  
`apt-get install python3 -y`  
`apt-get install python3-venv -y`  
`python3 -m venv venv`  
`source /usr/local/src/scripts/checklimit/venv/bin/activate`

Установить библиотеки, сделать файлы исполняемыми, выйти из виртуального окружения:  
`pip install -r requirements.txt`  
`chmod +x check*`  
`chmod +x banpeer.py`  
`chmod +x report.py`  
`deactivate`  

В файле `settings.py` указать корректные ключ и хеш API (смотреть в дашборде управления сервером), токен телеграм бота и ID чата с ним:  
`key = <KEY>`  
`value = <HASH_KEY>`  
`bot = <TOKEN>`  
`chat_id = <CHAT_ID>`  

Создать файл для сбора логов о забаненых пирах: 
`touch /var/log/wgban.log`

В файле `/usr/local/src/scripts/wg-log.sh` нужно добавить строчку. Открыть файл на редактирование:  
`cd ..`  
`vim wg-log.sh`  
Вставить это:  
`/usr/local/src/scripts/checklimit/banpeer.py`  

Создать скрипт, который будет перезапускать в 00:00 службу wireguard:  
`touch restart-wg.sh && vim restart-wg.sh`  
Вставить это:  
`#!/bin/bash`  
`systemctl restart wg-quick@wg0 >> /var/log/wg.log 2>> /var/log/wg-error.log`  

Добавить в cron новые задания:  
`crontab -e`  
Вставить это:  
`0 0 * * * /usr/local/src/scripts/restart-wg.sh`  
`0 9 * * * /usr/local/src/scripts/checklimit/report.py`  

Перезапустить службу cron:  
`systemctl restart cron`  
`systemctl status cron`  


## Настройка клиентской части
### Установить программу на устройтво
Скачать и установить [дистрибутив программы ](https://www.wireguard.com/install/)  

### Запустить программу и отсканировать QR-код
После запуска скрипта add_user.sh в телеграм бот отправит QR-код с конфигурацией клиента.  
Необходимо запустить программу, нажать на "+", выбрать вариант "сканировать QR-код", отсканировать его, вписать любое название конфига.
После этого можно пользоваться программой.  

## Создание на локальном устройстве файла clientN.conf руками   
Создать на клиентском устройстве файл clientN.conf, где N — номер клиента. Добавить в него следующее содержимое:  
`[Interface]`  
`PrivateKey = <PRIVATE KEY OF CLIENT1>`  
`Address = 10.123.0.3/24, fd42:42:42::3/80`  
`DNS = 1.0.0.1, 1.1.1.1, 2001:4860:4860::8888`  
` `  
`[Peer]`  
`PublicKey = <PUBLIC KEY OF SERVER>`  
`AllowedIPs = 0.0.0.0/0, ::/0`  
`Endpoint = <SERVER IP>:<PORT>`  
`PersistentKeepalive = 25`  

Вместо _PRIVATE KEY OF CLIENT1_ вставить приватный ключ клиента.  
Вместо _PUBLIC KEY OF SERVER_ вставить публичный ключ сервера.  
Вместо _SERVER IP:PORT_ вставить External-IP сервера OpenVZ и порт, который указан в wg0.conf  

_**Для клиента 2 нужно будет изменить Address. IPv4 — 10.123.0.4/24, IPv6 — fd42:42:42::4/80**_
