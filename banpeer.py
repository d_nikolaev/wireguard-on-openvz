#!/usr/local/src/scripts/checklimit/venv/bin/python
# -*- coding: utf-8 -*-
from checklimit import calclimit
from checkpeers import writepeerstofile
import os
import datetime

banlog = "/var/log/wgban.log"

def getlistpeers():
    """
    Рассчёт общего количества расходанного трафика каждым пиром и составление списка с информацией
    """
    writepeerstofile()
    peers = []
    with open('peers.txt') as file:
        lines = file.readlines()
        for line in lines:
            line = line.split(sep='\t')
            traffic = int(line[6])+int(line[7])
            ip = line[4]
            publickey = line[1]
            peer = []
            peer.append(ip)
            peer.append(publickey)
            peer.append(traffic)
            peers.append(peer)
        return peers

def banpeer():
    """
    Бан пира, превысевшего суточный лимит трафика
    """
    peers = getlistpeers()
    limit = int(calclimit())
    for peer in peers:
        ip = peer[0]
        publickey = peer[1]
        traffic = int(peer[2])
        if traffic >= limit:
            os.system("wg set wg0 peer " + publickey + " remove")
            now = datetime.datetime.now().strftime("%m-%d-%Y %H:%M:%S")
            trafficinGB = str(round(traffic / 1024 / 1024 / 1024, 2))
            limitinGB = str(round(limit / 1024 / 1024 / 1024, 2))
            message = now + " User " + publickey + " (ip: " + ip + ") banned. Limit: " + limitinGB + " GiB, Traffic: " + \
                trafficinGB + " GiB"
            print(message)
            os.system('echo "' + message + '" >> ' + banlog)
        else:
            pass

def main():
    banpeer()

if __name__ == '__main__':
    main()
