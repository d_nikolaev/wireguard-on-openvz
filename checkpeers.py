#!/usr/local/src/scripts/checklimit/venv/bin/python
# -*- coding: utf-8 -*-
import os

def writepeerstofile():
    """
    Запись в файл информации о пирах 
    """
    os.system("wg show all dump | grep 10.123 > peers.txt")

def checkpeers():
    """
    Отдаёт количество пиров в wireguard
    """
    writepeerstofile()
    peers = 0
    with open('peers.txt') as file:
        for peer in file:
            peers += 1
    return peers

def main():
    print("Num peers: " + str(checkpeers()))

if __name__ == '__main__':
    main()

