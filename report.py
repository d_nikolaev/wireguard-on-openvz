#!/usr/local/src/scripts/checklimit/venv/bin/python
# -*- coding: utf-8 -*-
from checklimit import getfreelimit
import os
import settings

bot = settings.bot
chat_id = settings.chat_id

def sentreport():
    """
    Отправляет в Telegram информацию о состоянии сервера, сколько осталось неиспользованного трафика
    """
    limit = str(round(getfreelimit() / 1024 / 1024 / 1024, 2))
    os.system("hostname > /usr/local/src/scripts/checklimit/temp.txt")
    with open('/usr/local/src/scripts/checklimit/temp.txt') as file:
        hostname = file.read().replace('\n','')
    date = str(os.system("date > /usr/local/src/scripts/checklimit/temp.txt"))
    with open('/usr/local/src/scripts/checklimit/temp.txt') as file:
        date = file.read().replace('\n','')
    os.system('curl -s -X POST https://api.telegram.org/bot' + bot + '/sendMessage' + \
        ' -d chat_id=' + chat_id + ' -d text="' + hostname + '\n' + date + '\n' + 'Freelimit: ' + limit + ' GiB"')
    os.system('rm -rf /usr/local/src/scripts/checklimit/temp.txt')


def main():
    sentreport()


if __name__ == '__main__':
    main()
